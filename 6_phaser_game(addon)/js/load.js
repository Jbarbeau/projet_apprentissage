/**
 * Created by jbarbeau on 17-02-13.
 */

var loadState = {
  preload: function(){
      //ajoute un libelle ecris chargement... sur l'ecran
      var loadingLabel = game.add.text(game.width/2,150,'chargement...',{font: '30px Arial', fill: '#ffffff'});
      loadingLabel.anchor.setTo(0.5,0.5);


      //Affiche la barre de progres en utilisant l'indice progressBar utiliser dans boot.js
      var progressBar = game.add.sprite(game.width/2,200,'progressBar');
      progressBar.anchor.setTo(0.5,0.5);
      game.load.setPreloadSprite(progressBar);

      // charge tout nos assets pour le jeu
      game.load.spritesheet('player','assets/player2.png', 20, 20);
      game.load.image('enemy','assets/enemy.png');
      game.load.image('coin','assets/coin.png');
      game.load.image('wallV','assets/wallVertical.png');
      game.load.image('wallH','assets/wallHorizontal.png');
      game.load.image('pixel','assets/pixel.png');
      game.load.spritesheet('mute','assets/muteButton.png',28,22);

      // charge le son
      //le son du joueur qui saute
      game.load.audio('jump',['assets/jump.ogg','assets/jump.mp3']);

      //le son du joueur qui prend une piece
      game.load.audio('coin',['assets/coin.ogg','assets/coin.mp3']);

      //le son du joueur qui meurt
      game.load.audio('dead',['assets/dead.ogg','assets/dead.mp3']);

      game.load.audio('music',['assets/music.ogg','assets/music.mp3']);
      game.load.audio('menu',['assets/menu.ogg','assets/menu.mp3']);

      //charge un nouvel asset que je vais utiliser dans le menu State
      game.load.image('background','assets/background.png');

  },

    create: function(){

      // Demarre le State menu.js
      game.state.start('menu');
    }
};
