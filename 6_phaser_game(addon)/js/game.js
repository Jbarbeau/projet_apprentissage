/**
 * Created by jbarbeau on 17-02-13.
 */
// initialisation de phaser
var game = new Phaser.Game(500, 340, Phaser.Auto, 'gameDiv');

//definition de nos variables globales
game.global = {
    score:0
};

//ajout de tout les autres State

game.state.add('boot', bootState);
game.state.add('load', loadState);
game.state.add('menu', menuState);
game.state.add('play', playState);

//demarre le State boot
game.state.start('boot');