/**
 * Created by jbarbeau on 17-02-13.
 */
var menuState = {
    create: function(){
        //ajoute une image de fond
        game.add.image(0,0,'background');

        this.music = game.add.audio('menu');// add the music
        this.music.loop = true;//make it loop
        this.music.play();//start the music

        //si 'bestScore' n;est pas defini
        //cela veux dire que c'est la premiere fois que le jeu est lancer
        if(!localStorage.getItem('bestScore')){
            //alors attribue 0 au score
            localStorage.setItem('bestScore',0);
        }

        //si le score est meilleur que le meilleur score enregistrer
        if(game.global.score > localStorage.getItem('bestScore')){
            // alors le system met le meilleur score a jour
            localStorage.setItem('bestScore',game.global.score);
        }

        //affiche le nom du jeu
        var nameLabel= game.add.text(game.width/2,-50,'Super Coin Box' , {font:'70px Geo', fill:'#ffffff'});
        nameLabel.anchor.setTo(0.5,0.5);

        game.add.tween(nameLabel).to({y: 80}, 1000)
            .easing(Phaser.Easing.Bounce.Out).start();



        //affiche le score au centre de l'ecran
        var text = 'score : ' + game.global.score + '\nbest score: ' + localStorage.getItem('bestScore');
        var scoreLabel = game.add.text(game.width/2,game.height/2, text, { font: '25px Arial', fill: '#ffffff', align: 'center'});
        scoreLabel.anchor.setTo(0.5,0.5);

        //text explicatif sur comment debuter le jeu
        var startLabel = game.add.text(game.width/2,game.height-80, ' press the up arrow key to start',{font: '25px Arial', fill:'#ffffff'});
        startLabel.anchor.setTo(0.5,0.5);

        //creation de l'interpolation
        game.add.tween(startLabel).to({angle: -2}, 500).to({angle: 2}, 1000).to({angle: 0},500).loop().start();

        //ajout du bouton qui fait appel a la fonction toggleSound
        this.muteButton = game.add.button(20,20,'mute',this.toggleSound,this);

        //si le jeu est deja sans son, afficher le speaker mute
        this.muteButton.frame = game.sound.mute ? 1 : 0;

        //cree une nouvelle variable Phaser keyboard : la fleche haut
        //lorsque pesee elle apelle Start
        var upKey = game.input.keyboard.addKey(Phaser.Keyboard.UP);
        upKey.onDown.add(this.start, this);



    },
    //fonction qui est appeler lorsque le boutton mute est enfoncer
    toggleSound: function(){
        //change la variable de vrai a faux ou vice versa
        //quand 'game.sound.mute = true', Phaser va muter le jeu
        game.sound.mute = !game.sound.mute;

        //change le frame du button
        this.muteButton.frame = game.sound.mute ? 1 : 0 ;
    },

    start: function(){
        // demarre le jeu
        //arrete la musique
        this.music.stop();
        game.state.start('play');
    }
};