/**
 * Created by jbarbeau on 17-02-13.
 */
var bootState = {
    preload : function () {
        //chargement de l'image
        game.load.image('progressBar', 'assets/progressBar.png')
    },

    create:function(){
      // initialisation des configuration de base
        game.stage.backgroundColor = '#3498db';
        game.physics.startSystem(Phaser.Physics.ARCADE);
        game.renderer.renderSession.roundPixels = true;

        //démarre la prochaine State aka load.js
        game.state.start('load');
    }
};