/**
 * Created by jbarbeau on 17-02-08.
 */

// Création de la seule STATE appeler ''mainState''
var playState = {


    create: function () {

        // Creation d'une variable globale 'player' dans notre état (x,y,'indice de reference')
        // this.player = game.add.sprite(250,170,'player');
        // utilisation de variable predefinie pour la position de notre protagoniste
        this.player = game.add.sprite(game.width / 2, game.height / 2, 'player');

        // Configuration des 'anchor point' au centre de notre sprite
        this.player.anchor.setTo(0.5, 0.5);


        // Precise a Phaser que le protagoniste va utiliser le moteur physique ARCADE
        game.physics.arcade.enable(this.player);

        // Ajoute une gravite verticale au protagoniste
        this.player.body.gravity.y = 500;

        // Ajout des controles (les fleches)
        this.cursor = game.input.keyboard.createCursorKeys();

        //Les 4 fleche sont directement capturer par phaser
        //elle ne son pas envoyer au navigateur donc limite les scroll non voulu
        game.input.keyboard.addKeyCapture(
            [Phaser.Keyboard.UP, Phaser.Keyboard.DOWN,
            Phaser.Keyboard.LEFT, Phaser.Keyboard.RIGHT]
        );

        //ajout des touche WAD en plus des touche curseur
        this.wasd = {
            up: game.input.keyboard.addKey(Phaser.Keyboard.W),
            left: game.input.keyboard.addKey(Phaser.Keyboard.A),
            right: game.input.keyboard.addKey(Phaser.Keyboard.D)

        };

        //cree l'animation de droite en effectuant une boucle sur les fram 1 et 2
        this.player.animations.add('right',[1,2],8,true);

        //cree l'animation de fauche en effectuant une boucle sur les frame 3 et 4
        this.player.animations.add('left', [3,4],8,true);

        //creation d'un emetteur avec 15 particule. aucun besoin de configurer le x y
        //puisque qu'on ne sait pas ou est l'explosion
        this.emitter = game.add.emitter(0,0,15);

        //ajoute l'image pixel a l'emetteur
        this.emitter.makeParticles('pixel');

        //configure la viterre x et y de l'emetteur entre -150 et 150
        //la vitesse va etre choisi de facon aleatoire entre -150 et 150 pour chacune des particule
        this.emitter.setYSpeed(-150,150);
        this.emitter.setXSpeed(-150,150);

        //redimensionne les particules en 2x -> 0 en 800ms
        //les parametre sont : startX, endX, startY, endY, duration
        this.emitter.setScale(2, 0, 2, 0, 800);

        //appel de la fonction pour construire le niveau (mur)
        this.createWorld();

        //affiche les collectibles
        this.coin = game.add.sprite(60,140,'coin');

        //ajoute le moteur physique au collectible
        game.physics.arcade.enable(this.coin);

        //configure le point d encre au centre
        this.coin.anchor.setTo(0.5,0.5);

        //initialise les son
        this.jumpSound = game.add.audio('jump');
        this.coinSound = game.add.audio('coin');
        this.deadSound = game.add.audio('dead');

        this.music = game.add.audio('music');// add the music
        this.music.loop = true;//make it loop
        this.music.play();//start the music
        this.music.fadeIn(2500);

        //affiche le score
        this.scoreLabel = game.add.text(30,30,'score:0',{font : '18px arial', fill: '#ffffff'});

        //initialise la variable de pointage a 0
        game.global.score=0;

        //creation dun groupe qui contiendra les ennemis et active les physique ARCADE
        this.enemies = game.add.group();
        this.enemies.enableBody = true;

        //creation de 10 ennemi dans le groupe avec l'image 'enemy'
        //les ennemis sont 'mort' par defaut donc il ne sont pas visible dans le jeu
        this.enemies.createMultiple(10,'enemy');

        // On appel 'addEnnemy' a chaque 2.2 sec
        //game.time.events.loop(2200,this.addEnemy,this);

        //contien le temps avant la prochaine iteration de l'ennemi
        this.nextEnemy = 0;



    },

    update: function () {
        // Cette fonction est appelée 60 fois par seconde
        // Cette section contien toute la logique de notre jeu

        //dit a Phaser que le joueur et les murs doivent entré en collision
        game.physics.arcade.collide(this.player,this.walls);

        //appel la fonction movePLayer 60 fois seconde
        this.movePlayer();

        // On veux savoir si le joueur et les collectible se touche, si oui on appele la fonction takeCoin()
        game.physics.arcade.overlap(this.player, this.coin, this.takeCoin, null, this);



        // fait en sorte que les ennemi et les l'environnement se touche
        game.physics.arcade.collide(this.enemies,this.walls);

        //appel la fonction pour tuer le joueur quand le joueur et un ennemi se touche
        game.physics.arcade.overlap(this.player,this.enemies,this.playerDie,null,this);

        //si le temps avant le 'nextEnemy' est passer
        if (this.nextEnemy < game.time.now){

            // definition des variables
            var start = 4000, end = 1000, score = 100;

            //formule pour rapetisser le delai entre l'apparition des ennemi over time
            // 4000 ms au debut et lentement devient 1000ms
            var delay = Math.max(start - (start-end) * game.global.score / score, end);


            //ajoute un nouvel ennemi et met a jour le temps inclu dans la var nextEnemy
            this.addEnemy();
            this.nextEnemy = game.time.now + delay;
        }


        if(!this.player.alive){
            return;
        }

        //verifie si le joueur est dans le monde, sinon on appele playerDie
        if(!this.player.inWorld){
            this.playerDie();
        }

    },

    // Finalement , c'est ici que je vais ajouter des fonctions personnalisée

    movePlayer: function () {
        // Si la fleche gauche est enfoncee
        if (this.cursor.left.isDown || this.wasd.left.isDown) {
            //bouge le personnage a gauche
            //la velocite est en pixel par seconde
            this.player.body.velocity.x = -200;

            //joue l'animation de gauche
            this.player.animations.play('left');
        }
        // Si la fleche droite est enfoncee
        else if (this.cursor.right.isDown || this.wasd.right.isDown) {
            //bouge le personnage a droite
            this.player.body.velocity.x = 200;

            //joue l'animation de droite
            this.player.animations.play('right');
        }

        // si aucun de ces deux bouton est enfonce
        else {
            //arret du mouvement
            this.player.body.velocity.x = 0;
            this.player.animations.stop(); //arrete l'animation
            this.player.frame = 0;// arrete la tete de lecture sur le frame 0
        }

        // si la fleche haut est enfonce et que le joueur est sur le sol
        if ((this.cursor.up.isDown || this.wasd.up.isDown) && this.player.body.touching.down) {
            //fait sauter le joueur
            this.player.body.velocity.y = -320;
            this.jumpSound.play();
        }

    },
    createWorld: function () {
        // Creation dun groupe qui va contenir tout les murs et activation des Arcade physics pour ceux ci
        this.walls = game.add.group();
        this.walls.enableBody = true;

        // Creation des 10 murs dans notre groupe walls

        game.add.sprite(0, 0, 'wallV', 0, this.walls); // gauche
        game.add.sprite(480, 0, 'wallV', 0, this.walls); // droite
        game.add.sprite(0, 0, 'wallH', 0, this.walls); // haut gauche
        game.add.sprite(300, 0, 'wallH', 0, this.walls); // haut droit
        game.add.sprite(0, 320, 'wallH', 0, this.walls); // fond gauche
        game.add.sprite(300, 320, 'wallH', 0, this.walls); // fond droit
        game.add.sprite(-100, 160, 'wallH', 0, this.walls); // milieux gauche
        game.add.sprite(400, 160, 'wallH', 0, this.walls); // Milieux droit

        // ajout des petite tablettes entre le milieu et le top
        var middleTop = game.add.sprite(100, 80, 'wallH', 0, this.walls);
        middleTop.scale.setTo(1.5, 1);
        var middleBottom = game.add.sprite(100, 240, 'wallH', 0, this.walls);
        middleBottom.scale.setTo(1.5, 1);

        // rend les murs inbougeable
        this.walls.setAll('body.immovable', true);

    },

    playerDie:function(){


        //configure la position de l'emetteur au dessus du joueur
        this.emitter.x = this.player.x;
        this.emitter.y = this.player.y;
        this.player.kill();

        //cree un flash blanc pendant 300ms
        //game.camera.flash(0xffffff,300);

        //cree un tremblement de camera (intensity, duration)
        game.camera.shake(0.02,300);

        //demarre l'emetteur avec l'explosion de 15 particule avec une vie de 800ms
        this.emitter.start(true,800,null,15);

        //joue le son
        this.deadSound.play();

        //arrete la musique
        this.music.fadeOut(2500);
        this.music.stop();




        //lorsque le joueur meurt , nous ramene au menu
        game.time.events.add(1000, this.startMenu, this)
    },
    startMenu: function() {
      game.state.start('menu');
    },

    //prendre note que cette fonction a 2 parametre (player,coin). ce sont les deux object qui se touche, ils sont autmatiquement envoyer par la fonction game.physics.arcade.overlap

    takeCoin: function(player,coin){

        // redimensionne la piece pour la rendre invisible
        this.coin.scale.setTo(0,0);

        // grossi la piece a son format d'origine apres 300ms
        game.add.tween(this.coin.scale).to({x:1, y:1},300).start();

        game.add.tween(this.player.scale).to({x:1.3,y:1.3}, 100).yoyo(true).start();

        //tue le coin et le fait disparaitre du jeu
        this.coin.kill();

        //joue le son
        this.coinSound.play();

        //augmente notre score de 5 points
        game.global.score += 5;

        //met a jour le libelle de pointage en utilisant la propriete text
        this.scoreLabel.text = 'score:' + game.global.score;

        //appel la fonction pour changer la position de la piece
        this.updateCoinPosition();
    },

    updateCoinPosition: function() {

        //stocke les position possible de la piece dans un array
        var coinPosition = [
            {x: 140, y: 60}, {x: 360, y: 60}, // deux possible pour l'etage du haut
            {x: 60, y: 140}, {x: 440, y: 140}, // tablettes du milieu
            {x: 130, y: 300}, {x: 370, y: 300} // etage du bas
        ];

        //enleve la position courante de la pieces du tableau
        //sinon la piece pourrais reapparaite deux fois au meme endroit
        //donc tant que i est plus petit que la longeur du tableau
        for(var i=0; i< coinPosition.length; i++){
            //si l'indice qui fait reference au tableau a la meme valeur X que celle de la pieces courante
            if(coinPosition[i].x == this.coin.x){
                //supprime la position equivalente dans le tableau
                coinPosition.splice(i,1);
            }
        }

        // choisi au hasard une position dans le tableau construit plus haut avec game.rnd.pick
        var newPosition = game.rnd.pick(coinPosition);

        //assigne la nouvelle position a la piece
        this.coin.reset(newPosition.x, newPosition.y);

    },

    addEnemy: function(){
        // va chercher le premier ennemi mort du groupe
        var enemy = this.enemies.getFirstDead();

        //s'il ny a pas d'ennemis mort (c'est a dire quil sont tout les 10 afficher dans leu jeu), ne fait rien
        if(!enemy){
            return;
        }

        // Initialisation de l'ennemi

        //configuration de l'ancre centrer au bas du sprite
        enemy.anchor.setTo(0.5,1);
        // met l'ennemi en haut du trou au plafond
        enemy.reset(game.width/2, 0);
        //ajout de la gravite pour le voir tomber
        enemy.body.gravity.y = 500;
        //ajout d'une velocite pour son mouvement et dun chiffre au hasard soit 1 ou -1 pour la direction
        enemy.body.velocity.x = 100 * game.rnd.pick([-1, 1]);
        //si l'ennemi touch un mur on peut le faire changer de direction avec la propriete bounce
        enemy.body.bounce.x = 1;
        //si les ennemis tombe dans le trou du bas, ils meurt
        enemy.checkWorldBounds = true;
        enemy.outOfBoundsKill = true;
    }
};