/**
 * Created by jbarbeau on 17-02-13.
 */
var menuState = {
    create: function(){
        //ajoute une image de fond
        game.add.image(0,0,'background');

        //affiche le nom du jeu
        var nameLabel= game.add.text(game.width/2,80,'Super Coin Box' , {font:'50px Arial', fill:'#ffffff'});
        nameLabel.anchor.setTo(0.5,0.5);

        //affiche le score au centre de l'ecran
        var scoreLabel = game.add.text(game.width/2,game.height/2, 'score: ' + game.global.score, { font: '25px Arial', fill: '#ffffff'});
        scoreLabel.anchor.setTo(0.5,0.5);

        //text explicatif sur comment debuter le jeu
        var startLabel = game.add.text(game.width/2,game.height-80, ' press the up arrow key to start',{font: '25px Arial', fill:'#ffffff'});
        startLabel.anchor.setTo(0.5,0.5);

        //cree une nouvelle variable Phaser keyboard : la fleche haut
        //lorsque pesee elle apelle Start
        var upKey = game.input.keyboard.addKey(Phaser.Keyboard.UP);
        upKey.onDown.add(this.start, this);
    },

    start: function(){
        // demarre le jeu
        game.state.start('play');
    }
};