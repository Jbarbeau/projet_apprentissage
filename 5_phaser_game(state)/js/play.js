/**
 * Created by jbarbeau on 17-02-08.
 */

// Création de la seule STATE appeler ''mainState''
var playState = {


    create: function () {

        // Creation d'une variable globale 'player' dans notre état (x,y,'indice de reference')
        // this.player = game.add.sprite(250,170,'player');
        // utilisation de variable predefinie pour la position de notre protagoniste
        this.player = game.add.sprite(game.width / 2, game.height / 2, 'player');

        // Configuration des 'anchor point' au centre de notre sprite
        this.player.anchor.setTo(0.5, 0.5);


        // Precise a Phaser que le protagoniste va utiliser le moteur physique ARCADE
        game.physics.arcade.enable(this.player);

        // Ajoute une gravite verticale au protagoniste
        this.player.body.gravity.y = 500;

        // Ajout des controles (les fleches)
        this.cursor = game.input.keyboard.createCursorKeys();

        //appel de la fonction pour construire le niveau (mur)
        this.createWorld();

        //affiche les collectibles
        this.coin = game.add.sprite(60,140,'coin');

        //ajoute le moteur physique au collectible
        game.physics.arcade.enable(this.coin);

        //configure le point d encre au centre
        this.coin.anchor.setTo(0.5,0.5);

        //affiche le score
        this.scoreLabel = game.add.text(30,30,'score:0',{font : '18px arial', fill: '#ffffff'});

        //initialise la variable de pointage a 0
        game.global.score=0;

        //creation dun groupe qui contiendra les ennemis et active les physique ARCADE
        this.enemies = game.add.group();
        this.enemies.enableBody = true;

        //creation de 10 ennemi dans le groupe avec l'image 'enemy'
        //les ennemis sont 'mort' par defaut donc il ne sont pas visible dans le jeu
        this.enemies.createMultiple(10,'enemy');

        // On appel 'addEnnemy' a chaque 2.2 sec
        game.time.events.loop(2200,this.addEnemy,this);

    },

    update: function () {
        // Cette fonction est appelée 60 fois par seconde
        // Cette section contien toute la logique de notre jeu

        //dit a Phaser que le joueur et les murs doivent entré en collision
        game.physics.arcade.collide(this.player,this.walls);

        //appel la fonction movePLayer 60 fois seconde
        this.movePlayer();

        // On veux savoir si le joueur et les collectible se touche, si oui on appele la fonction takeCoin()
        game.physics.arcade.overlap(this.player, this.coin, this.takeCoin, null, this);

        //verifie si le joueur est dans le monde, sinon on appele playerDie
        if(!this.player.inWorld){
            this.playerDie();
        }

        // fait en sorte que les ennemi et les l'environnement se touche
        game.physics.arcade.collide(this.enemies,this.walls);

        //appel la fonction pour tuer le joueur quand le joueur et un ennemi se touche
        game.physics.arcade.overlap(this.player,this.enemies,this.playerDie,null,this);


    },

    // Finalement , c'est ici que je vais ajouter des fonctions personnalisée

    movePlayer: function () {
        // Si la fleche gauche est enfoncee
        if (this.cursor.left.isDown) {
            //bouge le personnage a gauche
            //la velocite est en pixel par seconde
            this.player.body.velocity.x = -200;
        }
        // Si la fleche droite est enfoncee
        else if (this.cursor.right.isDown) {
            //bouge le personnage a droite
            this.player.body.velocity.x = 200;
        }

        // si aucun de ces deux bouton est enfonce
        else {
            //arret du mouvement
            this.player.body.velocity.x = 0;
        }

        // si la fleche haut est enfonce et que le joueur est sur le sol
        if (this.cursor.up.isDown && this.player.body.touching.down) {
            //fait sauter le joueur
            this.player.body.velocity.y = -320;
        }

    },
    createWorld: function () {
        // Creation dun groupe qui va contenir tout les murs et activation des Arcade physics pour ceux ci
        this.walls = game.add.group();
        this.walls.enableBody = true;

        // Creation des 10 murs dans notre groupe walls

        game.add.sprite(0, 0, 'wallV', 0, this.walls); // gauche
        game.add.sprite(480, 0, 'wallV', 0, this.walls); // droite
        game.add.sprite(0, 0, 'wallH', 0, this.walls); // haut gauche
        game.add.sprite(300, 0, 'wallH', 0, this.walls); // haut droit
        game.add.sprite(0, 320, 'wallH', 0, this.walls); // fond gauche
        game.add.sprite(300, 320, 'wallH', 0, this.walls); // fond droit
        game.add.sprite(-100, 160, 'wallH', 0, this.walls); // milieux gauche
        game.add.sprite(400, 160, 'wallH', 0, this.walls); // Milieux droit

        // ajout des petite tablettes entre le milieu et le top
        var middleTop = game.add.sprite(100, 80, 'wallH', 0, this.walls);
        middleTop.scale.setTo(1.5, 1);
        var middleBottom = game.add.sprite(100, 240, 'wallH', 0, this.walls);
        middleBottom.scale.setTo(1.5, 1);

        // rend les murs inbougeable
        this.walls.setAll('body.immovable', true);

    },

    playerDie:function(){

        //lorsque le joueur meurt , nous ramene au menu
        game.state.start('menu');
    },

    //prendre note que cette fonction a 2 parametre (player,coin). ce sont les deux object qui se touche, ils sont autmatiquement envoyer par la fonction game.physics.arcade.overlap

    takeCoin: function(player,coin){

        //tue le coin et le fait disparaitre du jeu
        this.coin.kill();

        //augmente notre score de 5 points
        game.global.score += 5;

        //met a jour le libelle de pointage en utilisant la propriete text
        this.scoreLabel.text = 'score:' + game.global.score;

        //appel la fonction pour changer la position de la piece
        this.updateCoinPosition();
    },

    updateCoinPosition: function() {

        //stocke les position possible de la piece dans un array
        var coinPosition = [
            {x: 140, y: 60}, {x: 360, y: 60}, // deux possible pour l'etage du haut
            {x: 60, y: 140}, {x: 440, y: 140}, // tablettes du milieu
            {x: 130, y: 300}, {x: 370, y: 300} // etage du bas
        ];

        //enleve la position courante de la pieces du tableau
        //sinon la piece pourrais reapparaite deux fois au meme endroit
        //donc tant que i est plus petit que la longeur du tableau
        for(var i=0; i< coinPosition.length; i++){
            //si l'indice qui fait reference au tableau a la meme valeur X que celle de la pieces courante
            if(coinPosition[i].x == this.coin.x){
                //supprime la position equivalente dans le tableau
                coinPosition.splice(i,1);
            }
        }

        // choisi au hasard une position dans le tableau construit plus haut avec game.rnd.pick
        var newPosition = game.rnd.pick(coinPosition);

        //assigne la nouvelle position a la piece
        this.coin.reset(newPosition.x, newPosition.y);

    },

    addEnemy: function(){
        // va chercher le premier ennemi mort du groupe
        var enemy = this.enemies.getFirstDead();

        //s'il ny a pas d'ennemis mort (c'est a dire quil sont tout les 10 afficher dans leu jeu), ne fait rien
        if(!enemy){
            return;
        }

        // Initialisation de l'ennemi

        //configuration de l'ancre centrer au bas du sprite
        enemy.anchor.setTo(0.5,1);
        // met l'ennemi en haut du trou au plafond
        enemy.reset(game.width/2, 0);
        //ajout de la gravite pour le voir tomber
        enemy.body.gravity.y = 500;
        //ajout d'une velocite pour son mouvement et dun chiffre au hasard soit 1 ou -1 pour la direction
        enemy.body.velocity.x = 100 * game.rnd.pick([-1, 1]);
        //si l'ennemi touch un mur on peut le faire changer de direction avec la propriete bounce
        enemy.body.bounce.x = 1;
        //si les ennemis tombe dans le trou du bas, ils meurt
        enemy.checkWorldBounds = true;
        enemy.outOfBoundsKill = true;
    }
};