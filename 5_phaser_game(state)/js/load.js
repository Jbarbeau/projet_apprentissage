/**
 * Created by jbarbeau on 17-02-13.
 */

var loadState = {
  preload: function(){
      //ajoute un libelle ecris chargement... sur l'ecran
      var loadingLabel = game.add.text(game.width/2,150,'chargement...',{font: '30px Arial', fill: '#ffffff'});
      loadingLabel.anchor.setTo(0.5,0.5);


      //Affiche la barre de progres en utilisant l'indice progressBar utiliser dans boot.js
      var progressBar = game.add.sprite(game.width/2,200,'progressBar');
      progressBar.anchor.setTo(0.5,0.5);
      game.load.setPreloadSprite(progressBar);

      // charge tout nos assets pour le jeu
      game.load.image('player','assets/player.png');
      game.load.image('enemy','assets/enemy.png');
      game.load.image('coin','assets/coin.png');
      game.load.image('wallV','assets/wallVertical.png');
      game.load.image('wallH','assets/wallHorizontal.png');

      //charge un nouvel asset que je vais utiliser dans le menu State
      game.load.image('background','assets/background.png');

  },

    create: function(){

      // Demarre le State menu.js
      game.state.start('menu');
    }
};
